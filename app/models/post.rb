class Post < ApplicationRecord
  mount_uploader :picture, PictureUploader

  validate :picture_size_validation

  def picture_size_validation
    errors[:picture] << "should be less than 100KB" if picture.size > 0.1.megabytes
  end
end
